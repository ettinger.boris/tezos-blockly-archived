import path from 'path';
import HtmlWebpackPlugin from 'html-webpack-plugin';
import ScriptExtHtmlWebpackPlugin from 'script-ext-html-webpack-plugin';
const CopyPlugin = require('copy-webpack-plugin');


export default {
	entry: path.join(__dirname, 'src/index.js'),
	output: {
		path: path.join(__dirname, 'dist'),
		filename: '[name].bundle.js'
	},
	devServer: {
		contentBase: './dist',
		port: 3000
	},
	module: {
		rules: [{
			test: /\.js$/,
			exclude: /(node_modules|bower_components)/,
			use: [{
				loader: 'babel-loader',
				options: {
					presets: [['@babel/preset-env', {
						'targets': {'browsers': ['last 2 version']}
					}]]
					}
				}]
		},
			{
        test: /\.css$/i,
        use: ['style-loader', 'css-loader'],
      },
			{
				test: /\.xml/,
				use: [{
					loader:'raw-loader'
				}]
			},
			{
				test: /\.(jpg|ico|png)/,
				use: [{
					loader: 'file-loader'
				}],
			}
		]
	},
	plugins: [
		new HtmlWebpackPlugin({
			title: 'Tezos-Blockly - Visual Smart Contract Editor for Tezos protocol',
			template: path.join(__dirname, 'src/index.template.html')
		}),
		new ScriptExtHtmlWebpackPlugin({
			defaultAttribute: 'defer'
		}),
		new CopyPlugin({
			patterns:
			[
				{ from: path.resolve(__dirname, './../../node_modules/blockly/media') ,
					to: path.resolve(__dirname, 'dist/media') },
				{ from: path.resolve(__dirname, './public') ,
					to: path.resolve(__dirname, 'dist/') }

			]
		})
	],
	stats: {
		colors: true
	},
	devtool: 'source-map'
};
