/**
 * @license
 * Copyright 2012 Google LLC
 * SPDX-License-Identifier: Apache-2.0
 */

/**
 * @fileoverview Generating ReLIGO for variable blocks.
 * @author fraser@google.com (Neil Fraser)
 */
'use strict';
import * as Blockly from 'blockly/core';
export default (Cls) => {
	return class extends Cls {
		variables_get(block) {
			// Variable getter.
			var code = this.variableDB_.getName(block.getFieldValue('VAR'),
				Blockly.VARIABLE_CATEGORY_NAME);
			return [code, this.ORDER_ATOMIC];
		}
	}
}
