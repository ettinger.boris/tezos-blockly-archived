import "core-js/stable";
import "regenerator-runtime/runtime";
import * as Blockly from 'blockly';
//import 'blockly/blocks';
import customBlocks from './blocks.json';
import ReLIGO from './religo.js';
import mainToolBoxStr from './main-toolbox.xml';
import arithmeticStr from './arithmetic.xml';
import pay1Str from './pay1.xml';
import safeSubStr from './safe-subtract.xml';
import Prism from 'prismjs';
const parser = new DOMParser();
const mainToolBox = parser.parseFromString(mainToolBoxStr, 'application/xml');
const arithmetic = parser.parseFromString(arithmeticStr, 'application/xml');
const pay1 = parser.parseFromString(pay1Str, 'application/xml');
const safeSub = parser.parseFromString(safeSubStr, 'application/xml');


document.addEventListener('DOMContentLoaded', () => {
	const mainToolBoxNode = document.importNode(mainToolBox.children[0], true);
	const arithmeticNode = document.importNode(arithmetic.children[0], true);
	const pay1Node = document.importNode(pay1.children[0], true);
	const safeSubNode = document.importNode(safeSub.children[0], true);
	Blockly.defineBlocksWithJsonArray(customBlocks);
	const workspace = Blockly.inject('blocklyDiv',
		{
			toolbox: mainToolBoxNode,
			media: 'media/',
		});
	let onresize = function(e) {
		let blocklyArea = document.getElementById('blocklyArea');
		let blocklyDiv = document.getElementById('blocklyDiv');

		// Compute the absolute coordinates and dimensions of blocklyArea.
		let element = blocklyArea;
		let x = 0;
		let y = 0;
		do {
			x += element.offsetLeft;
			y += element.offsetTop;
			element = element.offsetParent;
		} while (element);
		// Position blocklyDiv over blocklyArea.
		blocklyDiv.style.left = x + 'px';
		blocklyDiv.style.top = y + 'px';
		blocklyDiv.style.width = blocklyArea.offsetWidth + 'px';
		blocklyDiv.style.height = blocklyArea.offsetHeight + 'px';
		Blockly.svgResize(workspace);
	};
	window.addEventListener('resize', onresize, false);
	onresize();
	Blockly.svgResize(workspace);
	const contractEl = document.getElementById('contract');
	workspace.addChangeListener(() => {
		const prevContent = contractEl.textContent;
		const newContent = ReLIGO.workspaceToCode(workspace);
		if (prevContent !== newContent) {
			let intermediateContent = newContent.replace(/\n/g, "");
			intermediateContent = intermediateContent.replace(/;(?![;})])/g, ";\n").replace(/\s\s+/g, ' ').replace(/\|/g,'\n\t\t|').replace(/}\s*(?![;})])/g, "}\n");
			contractEl.textContent = intermediateContent;
			document.querySelector('#compiled').textContent = '';
			document.querySelector('#result').textContent = '';
			Prism.highlightElement(contractEl);
		}
	});
	const button = document.querySelector('#ligo');
	button.addEventListener('click', async () => {
		const contract = ReLIGO.workspaceToCode(workspace);
		try {
			const resp = await fetch('/api/compile-contract', {
				method: 'POST',
				headers: {
					'Accept': 'application/json',
					'Content-Type': 'application/json'
				},
				body: JSON.stringify({
					syntax: 'reasonligo',
					entrypoint: 'main',
					code: contract,
				}),
			});
			const response = await resp.json();
			document.querySelector('#compiled').textContent = response.result;
			document.querySelector('#result').innerHTML = resp.ok ? '<div class="alert alert-primary" role="alert">Success!</div>' : '<div class="alert alert-danger" role="alert">Failure...</div>';
		} catch {
			document.querySelector('#result').innerHTML = '<div class="alert alert-danger" role="alert">Failure...</div>';
		}
		;
	});
	function injectBlocks(index) {
		workspace.clear();
		let node = null;
		switch(index) {
			case 1:
				node = arithmeticNode;
				break;
			case 2:
				node = pay1Node;
				break;
			case 3:
				node = safeSubNode;
				break;
		}
		if (node !== null) {
			Blockly.Xml.domToWorkspace(node, workspace);
		}
	}
	const select = document.querySelector('#sel1');
	select.addEventListener('change', () => injectBlocks(select.selectedIndex, Blockly))
})
