FROM node:12-buster

RUN apt-get update && apt-get -y install perl6 libev-dev perl pkg-config libgmp-dev libhidapi-dev m4 libcap-dev bubblewrap rsync

RUN wget https://ligolang.org/deb/ligo.deb -O /tmp/ligo_deb10.deb

RUN dpkg -i /tmp/ligo_deb10.deb && rm /tmp/ligo_deb10.deb


WORKDIR /app

COPY package.json package.json
COPY tsconfig.json tsconfig.json
COPY packages/server/package.json packages/server/package.json
COPY packages/tezos-blockly/package.json packages/tezos-blockly/package.json
RUN yarn install

COPY packages/server packages/server
COPY packages/tezos-blockly packages/tezos-blockly
RUN yarn workspaces run build


ENV LIGO_CMD ligo

WORKDIR /app

ENTRYPOINT [ "yarn", "start" ]
